﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExampleScriptTWO: MonoBehaviour
{
    private int EnemyDistance = 0;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyUp("space"))
        {
            EnemySearch();
        }
    }
    void EnemySearch()
    {
        for(int i = 0; i<5; i++)
        {
            EnemyDistance = Random.Range(1, 10);

            if(EnemyDistance >= 8)
            {
                print("an enemy is far away!");
            }
            if (EnemyDistance >= 4 && EnemyDistance >=7)
            {
                print("an enemy is a medium range!");
            }
            if (EnemyDistance >4 )
            {
                print("an enemy is very close by!");
            }
        }
    }
}
